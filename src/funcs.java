import java.util.Random;

public class funcs {
    static void print2dArray(int[][] nn) {
        for (int i = 0; i < nn.length; i++) {
            for (int j = 0; j < nn[0].length; j++) {
                System.out.print(nn[i][j] + "\t");
            }
            System.out.println();
        }
    }

    static void sortRev2dArr(int[][] x) {
        int arrl = x.length * x[0].length, tmp;
        for (int i = 0; i < arrl; i++) {
            for (int j = 0; j < x.length; j++) {
                for (int k = 0; k < x[0].length - 1; k++) {
                    if (x[j][k + 1] > x[j][k]) {
                        tmp = x[j][k + 1];
                        x[j][k + 1] = x[j][k];
                        x[j][k] = tmp;
                    }
                }
                if (j != x.length - 1 && x[j + 1][0] > x[j][x[0].length - 1]) {
                    tmp = x[j][x[0].length - 1];
                    x[j][x[0].length - 1] = x[j + 1][0];
                    x[j + 1][0] = tmp;
                }
            }
        }
    }

    static void sort2dArr(int[][] x) {
        int arrl = x.length * x[0].length, tmp;
        for (int a = 0; a < arrl; a++) {
            for (int i = 0; i < x.length; i++) {
                for (int j = 0; j < x[0].length - 1; j++) {
                    if (x[i][j] > x[i][j + 1]) {
                        tmp = x[i][j + 1];
                        x[i][j + 1] = x[i][j];
                        x[i][j] = tmp;
                    }
                }
                if (i != (x.length - 1) && x[i + 1][0] < x[i][x[0].length - 1]) {
                    tmp = x[i + 1][0];
                    x[i + 1][0] = x[i][x[0].length - 1];
                    x[i][x[0].length - 1] = tmp;
                }
            }
        }
    }

    static void sortArr(int[] x) {
        int min, min_i;
        for (int i = 0; i < x.length; i++) {
            min = x[i];
            min_i = i;
            for (int j = i + 1; j < x.length; j++) {
                if (min > x[j]) {
                    min = x[j];
                    min_i = j;
                }
            }
            if (min_i != i) {
                x[min_i] = x[i];
                x[i] = min;
            }
        }
    }

    static int[][] make2dArray(int a) {
        Random random = new Random();
        int[][] aa = new int[a][a];
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < a; j++) {
                aa[i][j] = random.nextInt(99);
            }
        }
        return aa;
    }

    static int[] makeArray(int n) {
        Random random = new Random();
        int[] x = new int[n];
        for (int i = 0; i < n; i++) {
            x[i] = random.nextInt(98) + 1;
        }
        return x;
    }

    static int colsSum(int[][] x, int a) {
        int sum = 0;
        for (int j = 0; j < x[0].length; j++)
            sum += x[j][a];
        return sum;
    }

    static void changeCols(int[][] x, int a, int b) {
        int tmp;
        for (int i = 0; i < x.length; i++) {
            tmp = x[i][a];
            x[i][a] = x[i][b];
            x[i][b] = tmp;
        }
    }

    static void changeRows(int[][] x, int a, int b) {
        int tmp;
        for (int i = 0; i < x.length; i++) {
            tmp = x[a][i];
            x[a][i] = x[b][i];
            x[b][i] = tmp;
        }
    }

    static void sortArrCols(int[][] x) {
        for (int i = 0; i < x.length; i++) {
            for (int j = i + 1; j < x[0].length; j++) {
                if (colsSum(x, i) > colsSum(x, j)) {
                    changeCols(x, i, j);
                }
            }
        }
    }

    static void sortRevArrCols(int[][] x) {
        for (int i = 0; i < x.length; i++) {
            for (int j = i + 1; j < x[0].length; j++) {
                if (colsSum(x, i) < colsSum(x, j)) {
                    changeCols(x, i, j);
                }
            }
        }
    }

    static void sortArrRows(int[][] x) {
        for (int i = 0; i < x.length; i++) {
            for (int j = i + 1; j < x[0].length; j++) {
                if (rowsSum(x, i) > rowsSum(x, j)) {
                    changeRows(x, i, j);
                }
            }
        }
    }

    static void sortRevArrRows(int[][] x) {
        for (int i = 0; i < x.length; i++) {
            for (int j = i + 1; j < x[0].length; j++) {
                if (rowsSum(x, i) < rowsSum(x, j)) {
                    changeRows(x, i, j);
                }
            }
        }
    }

    static int[][] arrayCopy(int[][] x) {
        int a = x.length;
        int b = x[0].length;
        int[][] nn = new int[a][b];

        for (int i = 0; i < a; i++) {
            for (int j = 0; j < b; j++) {
                nn[i][j] = x[i][j];
            }
        }
        return nn;
    }

    static int rowsSum(int[][] x, int a) {
        int sum = 0;
        for (int i = 0; i < x.length; i++)
            if (isEven(x[a][i]))
                sum += x[a][i];
        return sum;
    }

    static boolean isEven(int x) {
        if (x % 2 == 0)
            return true;
        else
            return false;
    }

    static void printArrayColsSum(int[][] nn) {
        for (int i = 0; i < nn.length; i++) {
            for (int j = 0; j < nn[0].length; j++) {
                System.out.print(nn[i][j] + "\t");
            }
            System.out.println();
        }
        System.out.println();
        for (int k = 0; k < nn.length; k++) {
            System.out.print(funcs.colsSum(nn, k) + "\t");
        }
        System.out.println();
    }

    static void printArrayRowsSum(int[][] nn) {
        for (int i = 0; i < nn.length; i++) {
            for (int j = 0; j < nn[0].length; j++) {
                System.out.print(nn[i][j] + "\t");
            }
            System.out.print("| " + funcs.rowsSum(nn, i));
            System.out.println();
        }
    }

    static boolean isPrime(int x) {
        boolean flag = true;
        for (int i = 2; i < x; i++) {
            if (x % i == 0) {
                flag = false;
                break;
            }
        }
        return flag;
    }

    static int primeRowsCount(int[][] x) {
        int count = 0;
        for (int i = 0; i < x.length; i++)
            for (int j = 0; j < x[0].length; j++)
                if (isPrime(x[i][j])) {
                    count++;
                    break;
                }
        return count;
    }

    static int primeColsCount(int[][] x) {
        int count = 0;
        for (int i = 0; i < x.length; i++)
            for (int j = 0; j < x[0].length; j++)
                if (isPrime(x[j][i])) {
                    count++;
                    break;
                }
        return count;
    }

    static String word(int a, String[] words) {
        int word = 0;
        int n = a;
        if (n > 99)
            n = toTwo(n);

        if (n >= 21 && n <= 99)
            n = toOne(n);

        if (n >= 10 && n <= 20 || n >= 5 || n == 0)
            word = 2;
        else if (n >= 2 && n <= 4)
            word = 1;

        return a + " " + words[word];
    }

    static int toTwo(int a) {
        String s = Integer.toString(a);
        a = Integer.parseInt(s.substring(s.length() - 2));
        return a;
    }

    static int toOne(int a) {
        String s = Integer.toString(a);
        a = Integer.parseInt(s.substring(s.length() - 1));
        return a;
    }

    static int[][] Arrays2dMult(int[][] nn, int[][] mm) {
        int[][] nm = new int[nn.length][mm[0].length];
        if (nn[0].length == mm.length)
            for (int i = 0; i < nn[0].length; i++) {
                for (int j = 0; j < mm.length; j++) {
                    int tmp = 0;
                    for (int k = 0; k < mm.length; k++) {
                        tmp += nn[i][k] * mm[k][j];
                    }
                    nm[i][j] = tmp;
                }
            }
        else
            System.out.println("Эти матрицы нельзя умножить, тк кол-во столбцов матрицы A отличается от кол-ва строк матрицы B.");
        return nm;
    }

}

