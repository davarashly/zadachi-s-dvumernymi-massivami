public class Ex1 {
    public static void main(String[] args) {
        int a = 5;
        int[][] pp = funcs.make2dArray(a);
        int[][] nn = funcs.arrayCopy(pp);
        System.out.println("Наш массив");
        funcs.print2dArray(nn);
        funcs.sort2dArr(nn);
        System.out.println("\n#1");
        System.out.println("Наш отсортированный массив по возрастанию");
        funcs.print2dArray(nn);
        funcs.sortRev2dArr(nn);
        System.out.println("\n#2");
        System.out.println("Наш отсортированный массив по убыванию");
        funcs.print2dArray(nn);
        nn = funcs.arrayCopy(pp);
        funcs.sortArrCols(nn);
        System.out.println("\n#3");
        System.out.println("Наш массив, отсортированный по возрастанию сумм элементов в столбцах");
        funcs.printArrayColsSum(nn);
        funcs.sortRevArrCols(nn);
        System.out.println("\n#4");
        System.out.println("Наш массив, отсортированный по убыванию сумм элементов в столбцах");
        funcs.printArrayColsSum(nn);
        nn = funcs.arrayCopy(pp);
        funcs.sortArrRows(nn);
        System.out.println("\n#5");
        System.out.println("Наш массив, отсортированный по возрастанию суммы четных элементов в строках");
        funcs.printArrayRowsSum(nn);
        nn = funcs.arrayCopy(pp);
        funcs.sortRevArrRows(nn);
        System.out.println("\n#6");
        System.out.println("Наш массив, отсортированный по убыванию суммы четных элементов в строках");
        funcs.printArrayRowsSum(nn);
        nn = funcs.arrayCopy(pp);
        System.out.println("\n#7");
        System.out.println("В массиве " + funcs.word(funcs.primeRowsCount(nn), new String[]{"строка, содержащая", "строки, содержащие", "строк, содержащих"}) + " простые числа");
        nn = funcs.arrayCopy(pp);
        System.out.println("\n#8");
        System.out.println("В массиве " + funcs.word(funcs.primeColsCount(nn), new String[]{"столбец, содержаший", "столбца, содержащих", "столбцов, содержащих"}) + " простые числа");
        int[][] mm = funcs.make2dArray(a);
        nn = funcs.arrayCopy(pp);
        System.out.println("\n#9");
        System.out.println("Умножим наш массив на данный");
        funcs.print2dArray(mm);
        System.out.println("Получим массив");
        funcs.print2dArray(funcs.Arrays2dMult(nn, mm));
    }
}
